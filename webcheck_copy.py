#!/usr/bin/env python
#
# webcheck_copy
#
# Copyright (C) 2015 Michal Belica <devel@beli.sk>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
import json
import argparse
from pprint import pprint

from zapi import ZabbixAPI


def objlookup(seq, attr, match, get):
    try:
        ret = [x[get] for x in seq if attr in x and get in x and x[attr] == match][0]
    except IndexError:
        ret = None
    return ret

parser = argparse.ArgumentParser()
parser.add_argument('-u', '--url')
parser.add_argument('-l', '--login')
parser.add_argument('-p', '--passwd')
parser.add_argument('-t', '--template')
parser.add_argument('-f', '--file')

parser.add_argument('-U', '--url2')
parser.add_argument('-L', '--login2')
parser.add_argument('-P', '--passwd2')
parser.add_argument('-T', '--template2')
parser.add_argument('-F', '--file2')

parser.add_argument('-d', '--debug', action='store_true')
parser.add_argument('-n', '--dry-run', action='store_true')

args = parser.parse_args()

if args.file:
    with open(args.file, 'r') as f:
        data = json.loads(f.read())
    webchecks = data['webchecks']
    applications = data['applications']
else:
    zapi = ZabbixAPI(url=args.url, user=args.login, password=args.passwd)
    zapi.login()
    templateid = zapi.Template.find({'name': args.template}, attr_name='templateid')
    print 'TemplateID:', templateid
    webchecks = zapi.Httptest.get({'hostids': templateid, 'output': 'extend', 'selectSteps': 'extend'})
    applications = zapi.Application.get({'hostids': templateid, 'output': 'extend'})

if args.file2:
    data = {
            'webchecks': webchecks,
            'applications': applications,
            }
    with open(args.file2, 'w') as f:
        f.write(json.dumps(data, indent=2))
else:
    zapi2 = ZabbixAPI(url=args.url2, user=args.login2, password=args.passwd2)
    zapi2.login()
    if args.template2 is not None:
        template2id = zapi2.Template.find({'name': args.template2}, attr_name='templateid')
        print 'Template2ID:', template2id
    else:
        print 'Using Template2ID from source.'
        template2id = None
    applications2 = zapi2.Application.get({'hostids': template2id, 'output': 'extend'})

    for wc in webchecks:
        if template2id is not None:
            wc[u'hostid'] = template2id[0]
        appname = objlookup(applications, u'applicationid', wc[u'applicationid'], u'name')
        if appname is not None:
            wc[u'applicationid'] = objlookup(applications2, u'name', appname, u'applicationid')
        else:
            del wc[u'applicationid']
        del wc[u'httptestid']
        for step in wc[u'steps']:
            del step[u'httpstepid']
            del step[u'httptestid']
    if args.debug:
        print '--- webcheck defs ---'
        pprint(webchecks)
        print '--- end of webcheck defs ---'
    if not args.dry_run:
        zapi2.Httptest.create(webchecks)

