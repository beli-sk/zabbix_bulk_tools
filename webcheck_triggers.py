#!/usr/bin/env python
#
# webcheck_triggers
#
# Copyright (C) 2015 Michal Belica <devel@beli.sk>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
import argparse
from pprint import pprint

from zapi import ZabbixAPI


def objlookup(seq, attr, match, get):
    try:
        ret = [x[get] for x in seq if attr in x and get in x and x[attr] == match][0]
    except IndexError:
        ret = None
    return ret

parser = argparse.ArgumentParser()
parser.add_argument('-u', '--url', required=True)
parser.add_argument('-l', '--login', required=True)
parser.add_argument('-p', '--passwd', required=True)
parser.add_argument('-t', '--template', required=True)
parser.add_argument('-d', '--debug', action='store_true')
parser.add_argument('-n', '--dry-run', action='store_true')
args = parser.parse_args()

zapi = ZabbixAPI(url=args.url, user=args.login, password=args.passwd)
zapi.login()

hostid = zapi.Template.find({'name': args.template}, attr_name='templateid')
print 'HostID:', hostid
webchecks = zapi.Httptest.get({'hostids': hostid, 'output': 'extend', 'selectSteps': 'extend'})

triggers = []

for wc in webchecks:
    print '{}'.format(wc['name'])
    trigger = {u'comments': u'',
        u'description': u'Failed Webcheck {}'.format(wc['name']),
        u'error': u'',
        u'expression': u'{%s:web.test.fail[%s].last()}<>0' % (args.template, wc['name']),
        u'priority': u'2',
        u'status': u'0',
        u'type': u'0',
        u'url': wc['steps'][0]['url'],
        }
    triggers.append(trigger)

if args.debug:
    print '--- trigger definitions ---'
    pprint(triggers)
    print '--- end of trigger definitions ---'
if not args.dry_run:
    zapi.Trigger.create(triggers)

